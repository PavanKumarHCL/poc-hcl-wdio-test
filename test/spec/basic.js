const assert = require('assert')

describe('Launch the Umbrella quote page', () => {
    it('should have the right title', () => {
        browser.url('https://umb-mod-docker.azurewebsites.net/');
        browser.maximizeWindow();
        const title = browser.getTitle(); 
        assert.strictEqual(title, 'Umbrella Quote');
        browser.pause(3000);
        });
});

describe('Need to move your Umbrella quote below',()=>{
    it('click on the start button on the page',()=>{
        let clickonStartButton = $('//button[@class="btn btn-primary"]');
        clickonStartButton.click();
        browser.pause(3000);
    });
    it('click on the add driver link',()=>{
        let clickonAdddriver =$('//button[@class="button-right btn btn-link"]');
        clickonAdddriver.click();
        browser.pause(5000);
        let firstName=$('#validationCustom01');
        firstName.setValue('xyz')
        
        let lastName=$('#validationCustom02');
        lastName.setValue('ABC');
        let license=$('//input[@id="Auto Information"]');
        license.setValue('0123654897');
        let date = $('//div[@class="form-row"]//div[@class="react-datepicker__input-container"]');
        date.click();
        let dateenter=$('.react-datepicker-ignore-onclickoutside');
        dateenter.setValue('01/01/2019');
        browser.pause(3000)
        let saveChanges=$('//button[contains(text(),"Save Changes")]');
        saveChanges.click();
        browser.pause(3000);
    });
    
    it('click on the next button',()=>{
        let clickonNextbutton=$('//button[contains(text(),"Next")]');
        clickonNextbutton.click();
        browser.pause(3000);
    });
});
describe('navigate to quote result page',()=>{
     it('select the quote amount', ()=>{
         let selectQuote=$('//button[@class="button-primary btn btn-primary"]');
            selectQuote.click();
         });
         it('click on the buy now button wait untill confirmation page occur',()=>{
             let clickBuynow =$('//button[contains(text(),"Buy Now")]');
             clickBuynow.click();
            browser.pause(3000);
         })
    });
   